#!/usr/bin/env bash

determine_current_style() {
    local light_style_start_time="8"
    local dark_style_start_time="18"

    local current_time_hours="$(date +%_H)"
    if [[ "$current_time_hours" -ge "$light_style_start_time" 
        && "$current_time_hours" -lt "$dark_style_start_time" ]]; then
        current_style="$LIGHT_STYLE"
    else
        current_style="$DARK_STYLE"
    fi
}

apply_to_alacritty() {
    local -r CONFIG_DIR="$HOME/.config/alacritty"
    local -r LIGHT_STYLE_THEME="gruvbox-material-light.toml"
    local -r DARK_STYLE_THEME="gruvbox-material-dark.toml"
    local current_style_theme=""

    if [[ "$current_style" = "$LIGHT_STYLE" ]]; then
        current_style_theme="$LIGHT_STYLE_THEME"
    else
        current_style_theme="$DARK_STYLE_THEME"
    fi

    local substitution='s|"[^"]*"|"'"$CONFIG_DIR/$current_style_theme"'"|g'
    sed -i --follow-symlinks "$substitution" "$CONFIG_DIR/theme.toml"
}

apply_to_tmux() {
    local -r CONFIG_TO_CHANGE="$HOME/.config/tmux/tmux.conf.local"
    local -r CONFIG_TO_RELOAD="$HOME/.config/tmux/tmux.conf"
    local -r LIGHT_STYLE_THEME="light"
    local -r DARK_STYLE_THEME="dark"
    local current_style_theme=""

    if [[ "$current_style" = "$LIGHT_STYLE" ]]; then
        current_style_theme="$LIGHT_STYLE_THEME"
    else
        current_style_theme="$DARK_STYLE_THEME"
    fi

    local prefix="@gruvbox-material_theme"
    local substitution='s/'"$prefix"'.*/'"$prefix"' "'"$current_style_theme"'"/g'
    sed -i --follow-symlinks "$substitution" "$CONFIG_TO_CHANGE"
    tmux source "$CONFIG_TO_RELOAD"
}

apply_to_ulauncher() {
    local -r CONFIG="$HOME/.config/ulauncher/settings.json"
    local -r LIGHT_STYLE_THEME="adwaita-gtk4"
    local -r DARK_STYLE_THEME="adwaita-gtk4-dark"
    local current_style_theme""

    if [[ "$current_style" = "$LIGHT_STYLE" ]]; then
        current_style_theme="$LIGHT_STYLE_THEME"
    else
        current_style_theme="$DARK_STYLE_THEME"
    fi

    local substitution='s/"adwaita.*/"'"$current_style_theme"'"/g'
    sed -i --follow-symlinks "$substitution" "$CONFIG"
    kill $(pgrep ulauncher)
}

apply_to_bat() {
    local -r CONFIG="$HOME/.config/bat/config"
    local -r LIGHT_STYLE_THEME="gruvbox-light"
    local -r DARK_STYLE_THEME="gruvbox-dark"
    local current_style_theme""

    if [[ "$current_style" = "$LIGHT_STYLE" ]]; then
        current_style_theme="$LIGHT_STYLE_THEME"
    else
        current_style_theme="$DARK_STYLE_THEME"
    fi

    local prefix="--theme="
    local substitution='s/'"$prefix"'.*/'"$prefix"'"'"$current_style_theme"'"/g'
    sed -i --follow-symlinks "$substitution" "$CONFIG"
}

readonly LIGHT_STYLE="light"
readonly DARK_STYLE="dark"
current_style=""

determine_current_style
apply_to_alacritty
apply_to_tmux
apply_to_ulauncher
apply_to_bat
